import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../../provider/weather.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {
weatherdetalis:any;
  constructor(public weather:WeatherService) { }

  ngOnInit(): void {
    this.weather.getweather().subscribe(res=>{
      res.main.temp=res.main.temp - 273.15
      this.weatherdetalis=res;
      console.log(res,"weather")
      // 305K − 273.15 
    })
  }

}
