import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(public http:HttpClient) { }
  
  getweather():Observable<any>{
    let param=new HttpParams( )
    // param=param.append("callback","test");
   
    param=param.append("q",'Chennai,IN');
    param=param.append("appid",'0106c320fe5db2d5c894901bd05c2a41');
    
   
     let header={
    params:param
 }

 return this.http.get('https://api.openweathermap.org/data/2.5/weather',header)
  }

}
