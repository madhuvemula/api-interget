import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeatherComponent } from './page/weather/weather.component';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { WeatherService } from './provider/weather.service';
// import { BsDropdownModule} from 'ngx-bootstrap/dropdown';
// import { AlertModule } from 'ngx-bootstrap/alert';
// import { TooltipModule } from 'ngx-bootstrap/tooltip';
// import { ModalModule } from 'ngx-bootstrap/modal';


@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    // BsDropdownModule.forRoot(),
    // AlertModule.forRoot(),
    // TooltipModule.forRoot(),
    // ModalModule.forRoot(),
    AppRoutingModule,


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
